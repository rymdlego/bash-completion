# Install

```bash
sudo apt install bash-completion
sudo mkdir /etc/bash_completion.d
```

### check your .bashrc if you already source bash_completion. if not, you need to add it.
```bash
. /etc/bash_completion
```

### also run the above manually if you wish to initialize bash-completion in your current session.

### copy the ssh completion script
```bash
sudo cp ./ssh /etc/bash_completion.d/
```
